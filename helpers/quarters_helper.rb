require 'date'

module QuartersHelper
  Quarter = Struct.new(:id, :months, :ends_at_year, :ends_at_month, :ends_at_day) do
    def ends_at
      Date.new(ends_at_year, ends_at_month, ends_at_day)
    end

    def name
      "Q#{id}"
    end
  end

  def quarters
    quarters = [
      Quarter.new(1, [2, 3, 4], nil, 4, 30),
      Quarter.new(2, [5, 6, 7], nil, 7, 31),
      Quarter.new(3, [8, 9, 10], nil, 10, 31),
      Quarter.new(4, [11, 12, 1], nil, 1, 31)
    ]

    current_quarter = quarters.find { |quarter| quarter.months.include?(Date.today.month) }
    current_year = Date.today.year

    quarters.rotate!(quarters.index(current_quarter))

    quarters.each do |quarter|
      quarter.ends_at_year =
        if quarter.id < current_quarter.id || quarter.id == 4
          current_year + 1
        else
          current_year
        end
    end
    quarters
  end
end
